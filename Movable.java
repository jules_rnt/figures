public abstract class Movable
{
    protected int xPosition;
    protected int yPosition;
    
    protected abstract void draw() ;
    /**
     * Déplace horizontalement et lentement le triangle  de 'distanceH' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    protected void slowMoveHorizontal(int distance)
    {
        int delta;
        int steps = distance ;

        if(distance < 0) 
        {
            delta = -1;
            steps = -distance;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < steps; i++)
        {
            xPosition += delta;
            draw();
        }
    }

    /**
     * Déplace horizontalement et lentement le triangle  de 'distanceV' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    protected void slowMoveVertical(int distance)
    {
        int delta;
        int steps = distance ;

        if(distance < 0) 
        {
            delta = -1;
            steps = -distance;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < steps; i++)
        {
            yPosition += delta;
            draw();
        }
    }
}
