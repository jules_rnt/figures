import java.awt.*;

/**
 * Un triangle qui peut être manipulé et qui se dessine lui-même à l'écran.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.07.31
 */

public class Triangle extends Movable
{
    private int height;
    private int width;
    private String color;
    private boolean isVisible;

    /**
     * Crée un nouveau triangle à la position par défaut avec la couleur par défaut.
     *
     */
    public Triangle()
    {
        height = 60;
        width = 70;
        xPosition = 210;
        yPosition = 140;
        color = "green";
        isVisible = false;
    }

    /**
     * Rend ce triangle visible. Si il était déjà visible, ne fait rien.
     *
     */
    public void makeVisible()
    {
        isVisible = true;
        draw();
    }
    
    /**
     * Rend ce triangle visible. Si il était déjà visible, ne fait rien.
     *
     */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    /**
     * Déplace le triangle de quelques pixels à droite.
     *
     */
    public void moveRight()
    {
        moveHorizontal(20);
    }

    /**
     * Déplace le triangle de quelques pixels à gauche.
     *
     */
    public void moveLeft()
    {
        moveHorizontal(-20);
    }

    /**
     * Déplace le triangle de quelques pixels vers le haut.
     *
     */
    public void moveUp()
    {
        moveVertical(-20);
    }

    /**
     * Déplace le triangle de quelques pixels vers le bas.
     *
     */
    public void moveDown()
    {
        moveVertical(20);
    }

    /**
     * Déplace le triangle horizontalemnt de 'distance' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    public void moveHorizontal(int distance)
    {
        erase();
        xPosition += distance;
        draw();
    }

    /**
     * Déplace le triangle verticalement de 'distance' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    public void moveVertical(int distance)
    {
        erase();
        yPosition += distance;
        draw();
    }


    /**
     * Remplace la taille actuelle du triangle par la nouvelle taille.
     *
     * @param newHeight La nouvelle hauteur en pixels. Doit être &gt;= 0.
     * @param newWidth La nouvelle largeur en pixels. Doit être &gt;= 0.
     */
    public void changeSize(int newHeight, int newWidth)
    {
        erase();
        height = newHeight;
        width = newWidth;
        draw();
    }

    /**
     * Change la couleur.
     *
     * @param newColor La nouvelle couleur, les valeurs possibles sont "red",
     * "yellow", "blue", "green", "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }

    /**
     * Dessine le triangle à l'écran avec les caractéristiques actuelles.
     */
    protected void draw()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            int[] xpoints = { xPosition, xPosition + (width/2), xPosition - (width/2) };
            int[] ypoints = { yPosition, yPosition + height, yPosition + height };
            canvas.draw(this, color, new Polygon(xpoints, ypoints, 3));
            canvas.wait(10);
        }
    }

    /**
     * Retire le triangle de l'écran.
     */
    private void erase()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
